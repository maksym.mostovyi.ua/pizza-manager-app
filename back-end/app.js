const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
var cors = require('cors')

const orderRoutes = require('./routes/orderRoutes')

const app = express();

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use(cors())
app.options('*', cors())

app.use('/orders', orderRoutes);

app.listen(8080)

