const express = require('express');
const { body } = require('express-validator/check');
const ordersCtrl = require('../controllers/ordersCtrl');
const statsCtrl = require('../controllers/statsCtrl');

const router = express.Router();

router.post('/save', ordersCtrl.saveOrder);
router.get('/getAll', ordersCtrl.getOrders);
router.get('/getHistory', statsCtrl.getOrdersHistory);
router.get('/getStats', statsCtrl.getStats);
router.get('/getChart', statsCtrl.getChart);

module.exports = router;
