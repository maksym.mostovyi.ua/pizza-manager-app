const fs = require('fs');
const path = require('path');

exports.saveOrder = (req, res, next) => {
    const p = path.join(
        path.dirname(process.mainModule.filename),
        'data',
        'pizzaOrders.json'
    );

    fs.readFile(p, (err, fileContent) => {
        let orders = [];
        if (!err) {
            orders = JSON.parse(fileContent);
        }
        orders.push(req.body);
        fs.writeFile(p, JSON.stringify(orders),  err => {
            console.log(err);
        });
        res.status(200).json({ message: 'ok' })
        console.log('Order was saved !')
    });
};

exports.getOrders = (req, res, next) => {
    const p = path.join(
        path.dirname(process.mainModule.filename),
        'data',
        'pizzaOrders.json'
    );

    fs.readFile(p, (err, fileContent) => {
        let orders = [];
        if (!err) {
            orders = JSON.parse(fileContent);
        }
        res.json(orders);
    });
};
