const fs = require('fs');
const path = require('path');

exports.getOrdersHistory = (req, res, next) => {
    const p = path.join(
        path.dirname(process.mainModule.filename),
        'data',
        'historyData.json'
    );

    fs.readFile(p, (err, fileContent) => {
        let ordersHistory = [];
        if (!err) {
            ordersHistory = JSON.parse(fileContent);
        }
        res.json(ordersHistory);
    });
};

exports.getStats = (req, res, next) => {
    res.json({
        totalOrders: 150,
        ordersDelivered: 120,
        pendingOrders: 30,
        totalSales: 14543,
        ontime:29.7,
        late:53.4,
        performance:0.05
    });
};

exports.getChart = (req, res, next) => {
    const p = path.join(
        path.dirname(process.mainModule.filename),
        'data',
        'pizzaOrders.json'
    );

    const completed = [];
    const pending = [];
    const cancelled = [];

    fs.readFile(p, (err, fileContent) => {
        let pizzaOrders = [];
        if (!err) {
            pizzaOrders = JSON.parse(fileContent);
            pizzaOrders.forEach(order => {
                if(order.data.status === 'Completed') {
                    completed.push(order)
                } else if(order.data.status === 'Cancelled') {
                    cancelled.push(order)
                } else {
                    pending.push(order)
                }
            })
            // Artificial values used for better demo
            res.json({
                chartData: [
                    {
                        type: 'completed',
                        value: 440
                        // value: completed.length
                    },
                    {
                        type: 'pending',
                        value: 190
                        // value: pending.length
                    },
                    {
                        type: 'cancelled',
                        value: 130
                        // value: cancelled.length
                    }
                ]
            });
        }
    })
};
