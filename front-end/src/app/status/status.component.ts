import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {RequestsService} from "../shared/requests.service";
import {Order} from "../shared/models/order.model";
import {Chart} from "../shared/models/order.model";

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from 'html-to-pdfmake';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {

  constructor(private requestService: RequestsService) { }

  @ViewChild('pdfTable') pdfTable: ElementRef | undefined;

  data: Order[] = [];
  chartData: Chart[] = [];
  completed = 0;
  pending = 0;
  cancelled = 0;

  ngOnInit(): void {
    setTimeout(() => {
      this.getChartData();
    }, 500)
    this.getOrders();
  }

  getChartData() {
    this.requestService.getChartData().then(data => {
      this.chartData = data.chartData;
      this.completed = data.chartData[0].value;
      this.pending = data.chartData[1].value;
      this.cancelled = data.chartData[2].value;
    })
  }

  public downloadAsPDF() {
    const pdfTable = this.pdfTable?.nativeElement;
    const html = htmlToPdfmake(pdfTable.innerHTML);
    const documentDefinition = {content: html};
    pdfMake.createPdf(documentDefinition).open();
  }

  getOrders() {
    this.requestService.getOrders().then(data => {
      this.data = data;
    })
  }

  // Solution with orders manipulation made only for Demo purposes
  // without using back-end

  acceptOrder(index: number) {
    this.data[index].data.status = 'Accepted';
  }

  cancelOrder(index: number) {
    this.data[index].data.status = 'Cancelled';
    this.chartData[2].value += 1;
    this.cancelled += 1;
    this.pending = this.pending - 1;
  }

  completeOrder(index: number) {
    this.data[index].data.status = 'Completed';
    this.chartData[0].value += 1;
    this.completed += 1;
    this.pending = this.pending - 1;
  }

  timeSince(ts: any) {
    const date = new Date(ts);
    const intervals = [
      { label: 'year', seconds: 31536000 },
      { label: 'month', seconds: 2592000 },
      { label: 'day', seconds: 86400 },
      { label: 'hour', seconds: 3600 },
      { label: 'minute', seconds: 60 },
      { label: 'second', seconds: 1 }
    ];

    const seconds = Math.floor((Date.now() - date.getTime()) / 1000);
    const interval = intervals.find(i => i.seconds < seconds);
    const count = Math.floor(seconds / interval!.seconds);
    return `${count} ${interval!.label}${count !== 1 ? 's' : ''} ago`;
  }
}
