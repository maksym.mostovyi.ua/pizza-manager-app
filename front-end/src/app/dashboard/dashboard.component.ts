import { Component, OnInit } from '@angular/core';
import {RequestsService} from "../shared/requests.service";
import {LineChart} from "../shared/models/order.model";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  lineChartData: LineChart[] = [];

  delivered = 0;
  pending = 0;
  totalSale = 0;
  ontime = 0;
  late = 0;
  performance = 0;
  totalOrders = 0;

  constructor(private requestService: RequestsService) { }

  ngOnInit() {
    setTimeout(() => {
      this.getOrdersHistory();
    }, 500)

    this.getOrdersStats();
  }

  getOrdersHistory() {
    this.requestService.getOrdersHistory().then(data => {
      this.lineChartData.push(data[0]);
    })
  }

  getOrdersStats() {
    this.requestService.getOrdersStats().then(data => {
      this.delivered = data.ordersDelivered;
      this.pending = data.pendingOrders;
      this.totalSale = data.totalSales;
      this.ontime = data.ontime;
      this.late = data.late;
      this.performance = data.performance;
      this.totalOrders = data.totalOrders
    })
  }
}
