import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import {RequestsService} from "../shared/requests.service";
import {StatusesEnum} from "../shared/statuses.enum";
import {ToppingsEnum} from "../shared/topings.enum";
import{PizzaSize} from "../shared/topings.enum";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  dynamicForm: FormGroup | undefined;
  toppingsValues = ToppingsEnum;
  pizzaSize = 'Large';
  totalAmount: any = 0;
  orderSaved = false;

  constructor(private formBuilder: FormBuilder, private requestsService: RequestsService) { }

  ngOnInit(): void {
    this.dynamicForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      address: ['', Validators.required],
      number: ['', Validators.required],
      pizzaCount: new FormArray([])
    });
  }

  get f() { return this.dynamicForm?.controls; }
  get t() { return this.f?.pizzaCount as FormArray; }

  toggleSize(type: string, index: any): void {
    this.pizzaSize = type;
    // @ts-ignore
    this.t.controls[index].controls.size.value = type;
    this.t.value[index].size = type;
    this.calcTotal();
  }

  addPizza(): void {
    this.t.push(this.formBuilder.group({
      size: ['Large'],
      bacon: [false],
      papperoni: [false],
      mushroom: [false],
      olive: [false],
      basil: [false],
      sweetcorn: [false],
      onion: [false],
      tomato: [false],
    }));
    this.calcTotal();
  }

  removePizza(index: number): void {
    this.t.removeAt(index);
    this.calcTotal()
  }

  calcTotal() {
    this.totalAmount = 0;
    for(let i=0; i<this.t.value.length; i++) {
      for (const property in this.t.value[i]) {
        if(this.t.value[i][property] === true) {
          // @ts-ignore
          this.totalAmount += ToppingsEnum[property];
        }
      }
      this.totalAmount += PizzaSize[this.t.value[i].size]
    }
  }

  renderArtificialStatus(): string {
    const randomValue = Math.floor(Math.random() * 4) + 1
    return StatusesEnum[randomValue];
  }

  onSubmit() {
    const params = this.dynamicForm?.value;
    // Status field was added to be able to mock data at status view
    params.status = this.renderArtificialStatus();
    params.ts = new Date();
    // Solution just for demo purpose. Uuid can be used instead
    params.id = Math.floor(Math.random() * 10000) + 1;
    params.totalAmount = this.totalAmount;

    this.requestsService.saveOrder(params).then(() => {
      this.totalAmount = 0;
      this.t.clear();
      this.dynamicForm?.reset();
      this.orderSaved = true;
      setTimeout(() => {
        this.orderSaved = false;
      }, 1000)
    });
  }
}
