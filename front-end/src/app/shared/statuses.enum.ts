export enum StatusesEnum {
  New = 1,
  Completed = 2,
  Cancelled = 3,
  Transit = 4
}
