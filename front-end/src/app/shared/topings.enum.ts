export enum ToppingsEnum {
  bacon = 1,
  papperoni = 2,
  mushroom = 4,
  olive =  3,
  basil =  5,
  sweetcorn =  1,
  onion =  2,
  tomato =  1,
}

export enum PizzaSize {
  Large = 10.99,
  Medium = 6.99,
  Small = 3.99,
}
