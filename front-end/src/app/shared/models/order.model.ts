export interface Order {
  data: any;
  address: string;
  email: string;
  id: number;
  name: string;
  number: string;
  pizzaCount: object;
  status: string;
}

export interface LineChart {
  date: string
  total: number;
}

export interface Chart {
  type: string
  value: number;
}
