import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor() { }

  saveOrder(data: any) {
    return fetch(`http://localhost:8080/orders/save`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        data,
      })
    });
  }

  getOrders(): Promise<any> {
    return fetch(`http://localhost:8080/orders/getAll`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(res => {
      return res.json();
    });
  }

  getOrdersHistory(): Promise<any> {
    return fetch(`http://localhost:8080/orders/getHistory`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(res => {
      return res.json();
    });
  }

  getOrdersStats(): Promise<any> {
    return fetch(`http://localhost:8080/orders/getStats`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(res => {
      return res.json();
    });
  }

  getChartData(): Promise<any> {
    return fetch(`http://localhost:8080/orders/getChart`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(res => {
      return res.json();
    });
  }
}
