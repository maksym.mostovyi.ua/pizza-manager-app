import {Directive, Input, ElementRef, Injectable, OnInit} from '@angular/core';
import * as d3 from 'd3';

@Directive({
  selector: '[appLineChart]'
})

@Injectable({
  providedIn: 'root'
})

export class LineChartDirective implements OnInit {

  @Input() data: Array<any> | undefined;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.initPieChart();
  }

  initPieChart() {
    const element = this.element.nativeElement;
    const margin = {
      top: 20,
      bottom: 20,
      left: 50,
      right: 20
    };

    const width = 600 - margin.left - margin.right;
    const height = 300 - margin.top - margin.bottom;

    const createGradient = (select: any) => {
      const gradient = select
        .select('defs')
        .append('linearGradient')
        .attr('id', 'gradient')
        .attr('x1', '0%')
        .attr('y1', '100%')
        .attr('x2', '0%')
        .attr('y2', '0%');

      gradient.append('stop')
        .attr('offset', '0%')
        .attr('style', 'stop-color:#da6e2a;stop-opacity:0.05');

      gradient.append('stop')
        .attr('offset', '100%')
        .attr('style', 'stop-color:#da6e2a;stop-opacity:.5');
    }

    const createGlowFilter = (select: any) => {
      const filter = select
        .select('defs')
        .append('filter')
        .attr('id', 'glow')

      filter.append('feGaussianBlur')
        .attr('stdDeviation', '4')
        .attr('result', 'coloredBlur');

      const femerge = filter
        .append('feMerge');

      femerge.append('feMergeNode')
        .attr('in', 'coloredBlur');

      femerge.append('feMergeNode')
        .attr('in', 'SourceGraphic');
    }

    const svg = d3.select(element)
      .append('svg')
      .attr('width', 600 + margin.left + margin.right)
      .attr('height', 300 + margin.top + margin.bottom)
      .append('g')
      .attr('transform', `translate(${margin.left}, ${margin.top})`);

    svg.append('defs');
    svg.call(createGradient);
    svg.call(createGlowFilter);

    const parseTime = d3.timeParse('%Y/%m/%d');

    const parsedData = this.data?.map(order => ({
      ticker: order.ticker,
      values: order.values.map((val: any) => ({
        total: val.total,
        date: parseTime(val.date)
      }))
    }));

    const xScale = d3.scaleTime()
      .domain([
        d3.min(parsedData, (d:any) => d3.min(d.values, (v:any) => v.date)),
        d3.max(parsedData, (d:any) => d3.max(d.values, (v:any) => v.date))
      ])
      .range([0, width]);

    const yScale = d3.scaleLinear()
      .domain([
        d3.min(parsedData, (d:any) => d3.min(d.values, (v:any) => v.total)),
        d3.max(parsedData, (d:any) => d3.max(d.values, (v:any) => v.total))
      ])
      .range([height, 0]);

    const line = d3.line()
      .x((d:any) => xScale(d.date))
      .y((d:any) => yScale(d.total))
      .curve(d3.curveCatmullRom.alpha(0.5));
    svg.selectAll('.line')
      .data(parsedData)
      .enter()
      .append('path')
      .attr('d', (d:any) => {
        const lineValues = line(d.values).slice(1);
        const splitedValues = lineValues.split(',');

        return `M0,${height},${lineValues},l0,${height - splitedValues[splitedValues.length - 1]}`
      })
      .style('fill', 'url(#gradient)')

    svg.selectAll('.line')
      .data(parsedData)
      .enter()
      .append('path')
      .attr('d', (d:any) => line(d.values))
      .attr('stroke-width', '2')
      .style('fill', 'none')
      .style('filter', 'url(#glow)')
      .attr('stroke', '#da6e2a');

    const tick = svg.append('g')
      .attr('transform', `translate(0, ${height})`)
      .call(d3.axisBottom(xScale).ticks(12))
      .selectAll('.tick')
      .style('transition', '.2s');

    tick
      .selectAll('line')
      .attr('stroke-dasharray', `5, 5`)
      .attr('stroke', '#ccc')
      .attr('y2', `-${height}px`)

    tick
      .append('rect')
      .attr('width', `${(width / 12) + 10}px`)
      .attr('x', `-${width / 24 + 5}px`)
      .attr('y', `-${height}px`)
      .attr('height', `${height + 30}px`)
      .style('cursor', 'pointer')
      .style('fill', 'transparent');

    svg.select('.domain')
      .attr('stroke', '#ddd')
  }
}
