import {Directive, Input, ElementRef, Injectable, OnInit} from '@angular/core';
import * as d3 from 'd3';

@Directive({
  selector: '[appStackedChart]'
})

@Injectable({
  providedIn: 'root'
})

export class StackedChartDirective implements OnInit {

  @Input() data: Array<any> | undefined;

  total = 1560;

  private margin = {top: 10, right: 20, bottom: 0, left: 0};

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.initBarChart();
  }

  groupData() {
    let cumulative = 0
    const _data = this.data?.map(d => {
      cumulative += d.value
      return {
        value: d.value,
        cumulative: cumulative - d.value,
      }
    }).filter(d => d.value > 0)
    return _data;
  }

  initBarChart() {
    const element = this.element.nativeElement;
    const width = 800;
    const height = 20;
    const barHeight = 10;
    const w = width - this.margin.left - this.margin.right;
    const h = height - this.margin.top - this.margin.bottom;
    const halfBarHeight = barHeight / 2;
    const colors = ['#87d86b', '#4579fb', '#db2d54'];

    const svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', 23)
      .append('g')
      .attr('transform',
        'translate(' + this.margin.left + ',' + this.margin.top + ')');

    const x = d3
      .scaleLinear().domain([0, this.total])
      .range([0, w]);

    const _data = this.groupData()

    svg.selectAll('rect')
      .data(_data)
      .enter().append('rect')
      .attr('class', 'rect-stacked')
      .attr('x', (d: any) => x(d.cumulative))
      .attr('y', h / 2 - halfBarHeight)
      .attr('height', element.offsetHeight)
      .attr('width', (d: any) => x(d.value))
      .style('fill', (d: any, i: any) => colors[i])
  }
}
