import {Component, OnInit} from '@angular/core';
import { faCommentAlt } from '@fortawesome/free-regular-svg-icons';
import { faBell } from '@fortawesome/free-regular-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import {faThLarge} from "@fortawesome/free-solid-svg-icons";
import {faCreditCard} from "@fortawesome/free-regular-svg-icons";
import {faBoxOpen} from "@fortawesome/free-solid-svg-icons";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'front-end';
  faCommentAlt =faCommentAlt
  faBell =faBell
  faQuestionCircle = faQuestionCircle
  faThLarge = faThLarge
  faCreditCard = faCreditCard
  faBoxOpen = faBoxOpen

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.router.navigateByUrl('/dashboard');
  }
}
